DROP TABLE IF EXISTS `Panne`;
CREATE TABLE `Panne` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `Date` datetime NOT NULL,
  `TypePanne` enum('Reseau','Crash_disque','Memoire') NOT NULL,
  `idM` char(16) NOT NULL,
  `Tmachine` enum('Serveur','Pare-feux','Routeur') NOT NULL,
  `isRep` boolean NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
