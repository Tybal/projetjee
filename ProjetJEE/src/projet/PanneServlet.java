package projet;
 
import java.io.IOException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
/**
 * Servlet implementation class PanneServlet
 */

public class PanneServlet extends HttpServlet {
    private PanneService panneService= new PanneServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PanneServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String pageName = "/simulateur.jsp";
        RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
        try {
            rd.forward(request, response);
        } catch (ServletException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
 
    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doProcess(request, response);
    }
 
    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    	if (request.getParameter("Random") != null){
			panneService.randompanne();
		}
    	if (request.getParameter("Random10") != null){
			panneService.randompanne10();
		}
    	if (request.getParameter("Randomtemps") != null){
			panneService.randompannetemps();
		}
		
        String idM = request.getParameter("typemachine");
        String TypePanne = request.getParameter("typepanne");
        System.out.println(TypePanne);
        System.out.println(idM);
        panneService.ajoutPanne(idM, TypePanne);
        doGet(request, response);
    }
 
}