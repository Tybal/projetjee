package projet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 
 * @author Amine
 *
 */

public class PanneDAOImpl implements PanneDAO {

	public PanneDAOImpl(){
		super();
	}
	
	/**
	 * common method used to query DB
	 * 
	 * @param query
	 *            the SQL query to use
	 * @return a list of pannes built from the SQL query
	 */
	private List<Panne> findBy(String query) {
		Connection conn = null;
		List<Panne> listPannes = new ArrayList<Panne>();
		Statement stat = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getInstance().getConnection();
			if (conn != null) {
				stat = conn.createStatement();
				rs = stat.executeQuery(query);
				while (rs.next()) {
					int id = rs.getInt("id");
					Timestamp date = rs.getTimestamp("date");
					String TypePanne = rs.getString("TypePanne");
					String Tmachine = rs.getString("Tmachine");
					String idM = rs.getString("idM");
					Boolean panres = rs.getBoolean("isRep");
					listPannes.add(new Panne(id, date, TypePanne, Tmachine, idM, panres));
				}
			}
		} catch (Exception e) {
			// sert à afficher les potentielles erreurs
			e.printStackTrace();

		} finally {
			//libérer les resssources utilisées
			if (conn != null) {
				DBManager.getInstance().cleanup(conn, stat, rs);
			}
		}
		return listPannes;

	}



	public List<Panne> findByAll() {
		// avoid select * queries because of performance issues,
		// only query the columns you need
		return findBy("select id,date,TypePanne,idM,Tmachine,isRep from Panne ORDER BY date");
	}

	public List<Panne> findByMinute() {
		// watch out : this query is case sensitive. use upper function on last
		// minute
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 MINUTE)");

	}

	public List<Panne> findByHour() {
		// watch out : this query is case sensitive. use upper function on last
		// hour
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 HOUR)");

	}

	public List<Panne> findByDay() {
		// watch out : this query is case sensitive. use upper function on last
		// Day
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 DAY)");

	}

	public List<Panne> findByMonth() {
		// watch out : this query is case sensitive. use upper function on last
		// Month
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 MONTH)");

	}

	public List<Panne> findByTypenMonth() {
		// watch out : this query is case sensitive. use upper function on last
		// Month by type
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY Tmachine");

	}

	public List<Panne> findByTypenDay() {
		// watch out : this query is case sensitive. use upper function on last
		// Day by type
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 DAY) ORDER BY Tmachine");

	}

	public List<Panne> findByTypenHour() {
		// watch out : this query is case sensitive. use upper function on last
		// hour by type
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 HOUR) ORDER BY Tmachine");

	}

	public List<Panne> findByTypenMinute() {
		// watch out : this query is case sensitive. use upper function on last
		// minute
		//

		return findBy(
				"select id,date,TypePanne,idM,Tmachine,isRep from Panne where  date >= DATE_SUB(NOW(), INTERVAL 1 MINUTE) ORDER BY Tmachine");

	}

	
	
	public void mettreAJour(String id) {
		Connection conn = null;
		List<Panne> listPannes = new ArrayList<Panne>();
		PreparedStatement stat = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getInstance().getConnection();
			if (conn != null) {
				stat = conn.prepareStatement("UPDATE Panne SET isRep=true WHERE id=?;");
				stat.setString(1, id);
				int rowsUpdated = stat.executeUpdate();
			} 
		}catch (Exception e) {
				// TODO: handle exception : main exception should thrown to servlet
				// layer to display error message
				e.printStackTrace();

		} finally {
				// always clean up all resources in finally block
				if (conn != null) {
					DBManager.getInstance().cleanup(conn, stat, rs);
				}
		}
    }
	
	private String getRandomHexString(){
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        while(sb.length() <= 16){
            sb.append(Integer.toHexString(r.nextInt()));
        }

        return sb.toString().substring(0, 16);
    }
	
	public void randomPanne(){
		Connection conn = null;
		List<Panne> listPannes = new ArrayList<Panne>();
		PreparedStatement stat = null;
		ResultSet rs = null;
		String TPanne;
		String Tmachine;
		String idM ;
		int rowsUpdated;
		try {
			conn = DBManager.getInstance().getConnection();
			if (conn != null) {
		         	idM=getRandomHexString();
		         	Random rand = new Random();
		         	int j;
		         	j=rand.nextInt(3);
		         	if(j==1) {
		         		TPanne = "Memoire";
		         	}else if (j==2){
		         		TPanne = "Crash_disque";
		         	}      else {
		         		TPanne = "Reseau";
		         	}
		         	int k;
		         	k=rand.nextInt(3);
		         	if((k==1) && (j!=1) && (j!=2)){
		         		Tmachine = "Pare-feux";
		         	}else if (k==2 && (j!=1) && (j!=2)){
		         		Tmachine = "Routeur";
		         	}  	else {
		         		Tmachine = "Serveur";
		         	}
		         	stat = conn.prepareStatement("insert into Panne (Date, TypePanne, idM, Tmachine, isRep) Values (NOW(), ?,?,?,false)");
		         	stat.setString(1, TPanne);
		         	stat.setString(2, idM);
		         	stat.setString(3, Tmachine);
		         	rowsUpdated = stat.executeUpdate();
		         
				} 
			}catch (Exception e) {
					// TODO: handle exception : main exception should thrown to servlet
					// layer to display error message
					e.printStackTrace();
	
			} finally {
					// always clean up all resources in finally block
					if (conn != null) {
						DBManager.getInstance().cleanup(conn, stat, rs);
					}
			}
    
    }
    
	public void randomPanne10(){
		Connection conn = null;
		List<Panne> listPannes = new ArrayList<Panne>();
		PreparedStatement stat = null;
		ResultSet rs = null;
		String TPanne;
		String Tmachine;
		String idM ;
		int rowsUpdated;
		try {
			conn = DBManager.getInstance().getConnection();
			if (conn != null) {
					for(int i = 0; i<10;i++){
		         	idM=getRandomHexString();
		         	Random rand = new Random();
		         	int j;
		         	j=rand.nextInt(3) + 1;
		         	if(j==1) {
		         		TPanne = "Memoire";
		         	}else if (j==2){
		         		TPanne = "Crash_disque";
		         	}      else {
		         		TPanne = "Reseau";
		         	}
		         	int k;
		         	k=rand.nextInt(3) + 1;
		         	if((k==1) && (j!=1) && (j!=2)){
		         		Tmachine = "Pare-feux";
		         	}else if (k==2 && (j!=1) && (j!=2)){
		         		Tmachine = "Routeur";
		         	}  	else {
		         		Tmachine = "Serveur";
		         	}
		         	stat = conn.prepareStatement("insert into Panne (Date, TypePanne, idM, Tmachine, isRep) Values (NOW(), ?,?,?,false)");
		         	stat.setString(1, TPanne);
		         	stat.setString(2, idM);
		         	stat.setString(3, Tmachine);
		         	rowsUpdated = stat.executeUpdate();
		         }
				} 
			}catch (Exception e) {
					// TODO: handle exception : main exception should thrown to servlet
					// layer to display error message
					e.printStackTrace();
	
			} finally {
					// always clean up all resources in finally block
					if (conn != null) {
						DBManager.getInstance().cleanup(conn, stat, rs);
					}
			}
    
    }
	
	
	public void randomPannetemps(){
		new java.util.Timer().schedule(

			    new java.util.TimerTask() {
			        @Override
			        public void run() { 
							randomPanne();
			        }
			    }, 
			    60000 
			);
	}
	
	public void Ajout_Panne(String tmachine, String TypePanne) {
        Connection conn = null;
        PreparedStatement stat = null;
        Boolean message = false;
        String nom;
        System.out.println(TypePanne);
        System.out.println(tmachine);
        
        if(((TypePanne.equals("Reseau")) && (tmachine.equals("Serveur"))) |
        	((TypePanne.equals("Reseau")) && (tmachine.equals("Routeur"))) |
        	((TypePanne.equals("Reseau")) && (tmachine.equals("Pare-feux"))) |
        	((TypePanne.equals("Crash_disque")) && (tmachine.equals("Serveur"))) |
        	((TypePanne.equals("Memoire")) && (tmachine.equals("Serveur")))  )
        {
	        try {
	            conn = DBManager.getInstance().getConnection();
	            if (conn != null) {
	            	nom = getRandomHexString();
	            	if (verifierexiste(nom)!=null){
	            		while ((verifierexiste(nom)!=null) && (verifierexiste(nom).equals(TypePanne))){
	            			nom = getRandomHexString();
	            		}
	            	}
	                stat = conn.prepareStatement("insert into Panne values (null,NOW(),?,?,?,false);");
		         	stat.setString(1, TypePanne);
		         	stat.setString(2, nom);
		         	stat.setString(3, tmachine);
		         	int rowsUpdated = stat.executeUpdate();
		         	System.out.print(rowsUpdated);
	                message = true;
	            }
	        } catch (Exception e) {
	            message = false;
	        } finally {
	            if (conn != null) {
	                DBManager.getInstance().cleanup(conn, stat, null);
	            }
	        }
        }
       
	}
	
	public String verifierexiste(String nom){
		String type=null;
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		boolean fin = false;
		try {
			conn = DBManager.getInstance().getConnection();
			if (conn != null) {
				stat = conn.createStatement();
				rs = stat.executeQuery("selec");
				while ((rs.next()) && (fin==false)) {
					String idM = rs.getString("idM");
					if (idM.equals(nom)){
						type = rs.getString("Tmachine");
						fin=true;
					}
				}
			}
		} catch (Exception e) {
			// sert à afficher les potentielles erreurs
			e.printStackTrace();

		} finally {
			//libérer les resssources utilisées
			if (conn != null) {
				DBManager.getInstance().cleanup(conn, stat, rs);
			}
		}
		return type;

	}
	
	

}
