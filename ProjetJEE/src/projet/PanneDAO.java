package projet;

import java.util.List;

/**
 * 
 * @author Amine
 *
 */

public interface PanneDAO {
	/**
	 * find all pannes without any criteria
	 * 
	 * @return a list of pannes objects
	 */
	public List<Panne> findByAll();

	public List<Panne> findByMinute();

	public List<Panne> findByHour();

	public List<Panne> findByDay();

	public List<Panne> findByMonth();

	public List<Panne> findByTypenMonth();

	public List<Panne> findByTypenDay();

	public List<Panne> findByTypenHour();

	public List<Panne> findByTypenMinute();
	
	//fonctions diverses pour la generation de pannes
	
	public void mettreAJour(String id);
	
	public void randomPanne();
	
	public void randomPanne10();
	
	public void randomPannetemps();
	
	public void Ajout_Panne(String tmachine, String TypePanne);
	
	public String verifierexiste(String nom);

}