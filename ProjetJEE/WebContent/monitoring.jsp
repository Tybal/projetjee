<%@ page language = "java" 
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List, java.util.ArrayList,java.sql.Timestamp,java.util.Date,projet.Panne"
%>
<%
List<Panne> liste1 = (List<Panne>) request.getAttribute("listPannes1");
List<Panne> liste1t = (List<Panne>) request.getAttribute("listPannes1t");
List<Panne> liste2 = (List<Panne>) request.getAttribute("listPannes2");
List<Panne> liste2t = (List<Panne>) request.getAttribute("listPannes2t");
List<Panne> liste3 = (List<Panne>) request.getAttribute("listPannes3");
List<Panne> liste3t = (List<Panne>) request.getAttribute("listPannes3t");
List<Panne> liste4 = (List<Panne>) request.getAttribute("listPannes4");
List<Panne> liste4t = (List<Panne>) request.getAttribute("listPannes4t");
//List<Panne> liste1 = new ArrayList<Panne>();
//List<Panne> liste2 = new ArrayList<Panne>();
//List<Panne> liste3 = new ArrayList<Panne>();
//List<Panne> liste4 = new ArrayList<Panne>();
//Timestamp date1 = new Timestamp(93000000);
//liste1.add(new Panne(1,date1,"Reseau", "Serveur","846525454542454",false));
//liste2.add(new Panne(1,date1,"Reseau", "Serveur","846525454542454",false));
//liste3.add(new Panne(1,date1,"Reseau", "Serveur","846525454542454",false));
//liste4.add(new Panne(1,date1,"Reseau", "Serveur","846525454542454",false));
int compte1 = liste1.size();
int compte2 = liste2.size();
int compte3 = liste3.size();
int compte4 = liste4.size();
%>
<html>
<head>
<link href="./style/form_style2.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	$(function(){$('#parmin').hide();})
	$(function(){$('#parmintype').hide();})
	$(function(){$('#parheure').hide();})
	$(function(){$('#parheuretype').hide();})
	$(function(){$('#parjour').hide();})
	$(function(){$('#parjourtype').hide();})
	$(function(){$('#parmois').hide();})
	$(function(){$('#parmoistype').hide();})
    $("#detail1").click(function(){
        $("#parmin").toggle();
    });
    $("#detail1-t").click(function(){
        $("#parmintype").toggle();
    });
    $("#detail2").click(function(){
        $("#parheure").toggle();
    });
    $("#detail2-t").click(function(){
        $("#parheuretype").toggle();
    });
    $("#detail3").click(function(){
        $("#parjour").toggle();
    });
    $("#detail3-t").click(function(){
        $("#parjourtype").toggle();
    });
    $("#detail4").click(function(){
        $("#parmois").toggle();
    });
    $("#detail4-t").click(function(){
        $("#parmoistype").toggle();
    });
});


setTimeout(location.reload.bind(location), 300000);
</script>
<title>Console de Monitoring</title>

</head>
	<body>
	<h3>Console de Monitoring</h3>
	<div class="Mystere">
		
		
		<!-- premier -->
		<p>Il y a eu <%= compte1 %> Pannes depuis la derniere minute.</p>
		<button id="detail1">Details</button>
		<div id="parmin">
		<form action="MonitoringServlet" method="post">
		<table border="1" cellpadding="5" cellspacing="0">
			<tr>
				<th>id</th>
				<th>Date</th>
				<th>Type de panne</th>
				<th>Machine</th>
				<th>Type de machine</th>
				<th>Statut</th>
			</tr>
		<%	for(Panne panne:liste1){
				if (panne != null){
				int num = panne.getId();
				Date date =panne.getDate();
				String type = panne.getTypePanne();
				String machine = panne.getIdM();
				String typem = panne.getTmachine();
				String statut;
				if (panne.isPanRes()==false){
					statut="pas r�par�";
				} else {
					statut="r�par�";
				}
		%>
			<tr>
				<td><%= num %></td>
				<td><%= date %></td>
				<td><%= type %></td>
				<td><%= machine %></td>
				<td><%= typem %></td>
				<td><%= statut %>
				<%if (panne.isPanRes()==false){%>
				
					<input type="checkbox" name="repare" value="<%=num %>">
					
				</td>
				</tr>
					<%} %>
				<%} %>
			<%} %>
		</table>
		<input type="submit" value="envoi" name="envoyer"/>
		</form>
		</div>
		<button id="detail1-t">Details par type</button>
		<div id="parmintype">
		<form action="MonitoringServlet" method="post">
		<table border="1" cellpadding="5" cellspacing="0">
			<tr>
				<th>id</th>
				<th>Date</th>
				<th>Type de panne</th>
				<th>Machine</th>
				<th>Type de machine</th>
				<th>Statut</th>
			</tr>
		<%	for(Panne panne:liste1t){
				if (panne != null){
				int num = panne.getId();
				Date date =panne.getDate();
				String type = panne.getTypePanne();
				String machine = panne.getIdM();
				String typem = panne.getTmachine();
				String statut;
				if (panne.isPanRes()==false){
					statut="pas r�par�";
				} else {
					statut="r�par�";
				}
		%>
			<tr>
				<td><%= num %></td>
				<td><%= date %></td>
				<td><%= type %></td>
				<td><%= machine %></td>
				<td><%= typem %></td>
				<td><%= statut %>
				<%if (panne.isPanRes()==false){%>
				
				<input type="checkbox" name="repare" value="<%=num %>">	
				
				</td>
				</tr>
					<%} %>
				<%} %>
			<%} %>
		</table>
		<input type="submit" value="envoi" name="envoyer"/>
		</form>
		</div>
		
		<!-- deuxieme-->
		<p>Il y a eu <%= compte2 %> Pannes depuis la derniere heure.</p>
		<button id="detail2">Details</button>
		<div id="parheure">
		<form action="MonitoringServlet" method="post">
		<table border="1" cellpadding="5" cellspacing="0">
			<tr>
				<th>id</th>
				<th>Date</th>
				<th>Type de panne</th>
				<th>Machine</th>
				<th>Type de machine</th>
				<th>Statut</th>
			</tr>
		<%
			for(Panne panne:liste2){
				int num = panne.getId();
				Date date =panne.getDate();
				String type = panne.getTypePanne();
				String machine = panne.getIdM();
				String typem = panne.getTmachine();
				String statut;
				if (panne.isPanRes()==false){
					statut="pas r�par�";
				} else {
					statut="r�par�";
				}
		%>
			<tr>
				<td><%= num %></td>
				<td><%= date %></td>
				<td><%= type %></td>
				<td><%= machine %></td>
				<td><%= typem %></td>
				<td><%= statut %>
				<%if (panne.isPanRes()==false){%>
				
				<input type="checkbox" name="repare" value="<%=num %>">	
				</td>
				</tr>
				<%} %>
			<%} %>
		</table>
		</form>
		</div>
		<button id="detail2-t">Details par type</button>
		<div id="parheuretype">
		<form action="MonitoringServlet" method="post">
		<table border="1" cellpadding="5" cellspacing="0">
			<tr>
				<th>id</th>
				<th>Date</th>
				<th>Type de panne</th>
				<th>Machine</th>
				<th>Type de machine</th>
				<th>Statut</th>
			</tr>
		<%
			for(Panne panne:liste2t){
				int num = panne.getId();
				Date date =panne.getDate();
				String type = panne.getTypePanne();
				String machine = panne.getIdM();
				String typem = panne.getTmachine();
				String statut;
				if (panne.isPanRes()==false){
					statut="pas r�par�";
				} else {
					statut="r�par�";
				}
		%>
			<tr>
				<td><%= num %></td>
				<td><%= date %></td>
				<td><%= type %></td>
				<td><%= machine %></td>
				<td><%= typem %></td>
				<td><%= statut %>
				<%if (panne.isPanRes()==false){%>
				<input type="checkbox" name="repare" value="<%=num %>">	
				</td>
				</tr>
				<%} %>
			<%} %>
		</table>
		<input type="submit" value="envoi" name="envoyer"/>
		</form>
		</div>
		
		
		
		<!-- troisi�me -->
		
		<p>Il y a eu <%= compte3 %> Pannes depuis le dernier jour.</p>
		<button id="detail3">Details</button>
		<div id="parjour">
		<form action="MonitoringServlet" method="post">
		<table border="1" cellpadding="5" cellspacing="0">
			<tr>
				<th>id</th>
				<th>Date</th>
				<th>Type de panne</th>
				<th>Machine</th>
				<th>Type de machine</th>
				<th>Statut</th>
			</tr>
		<%
			for(Panne panne:liste3){
				int num = panne.getId();
				Date date =panne.getDate();
				String type = panne.getTypePanne();
				String machine = panne.getIdM();
				String typem = panne.getTmachine();
				String statut;
				if (panne.isPanRes()==false){
					statut="pas r�par�";
				} else {
					statut="r�par�";
				}
		%>
			<tr>
				<td><%= num %></td>
				<td><%= date %></td>
				<td><%= type %></td>
				<td><%= machine %></td>
				<td><%= typem %></td>
				<td><%= statut %>
				<%if (panne.isPanRes()==false){%>
				
				<input type="checkbox" name="repare" value="<%=num %>">	
					
				</td>
				</tr>
				<%} %>
			<%} %>
		</table>
		<input type="submit" value="envoi" name="envoyer"/>
		</form>
		</div>
		<button id="detail3-t">Details par type</button>
		<div id="parjourtype">
		<form action="MonitoringServlet" method="post">
		<table border="1" cellpadding="5" cellspacing="0">
			<tr>
				<th>id</th>
				<th>Date</th>
				<th>Type de panne</th>
				<th>Machine</th>
				<th>Type de machine</th>
				<th>Statut</th>
			</tr>
		<%
			for(Panne panne:liste3t){
				int num = panne.getId();
				Date date =panne.getDate();
				String type = panne.getTypePanne();
				String machine = panne.getIdM();
				String typem = panne.getTmachine();
				String statut;
				if (panne.isPanRes()==false){
					statut="pas r�par�";
				} else {
					statut="r�par�";
				}
		%>
			<tr>
				<td><%= num %></td>
				<td><%= date %></td>
				<td><%= type %></td>
				<td><%= machine %></td>
				<td><%= typem %></td>
				<td><%= statut %>
				<%if (panne.isPanRes()==false){%>>
				<input type="checkbox" name="repare" value="<%=num %>">		
				</td>
				</tr>
				<%} %>
			<%} %>
		</table>
		<input type="submit" value="envoi" name="envoyer"/>
		</form>
		</div>
		
		<!-- quatrieme -->
		
		<p>Il y a eu <%= compte4 %> Pannes depuis le dernier mois.</p>
		<button id="detail4">Details</button>
		<div id="parmois">
			<form action="MonitoringServlet" method="post">
			<table border="1" cellpadding="5" cellspacing="0">
				<tr>
					<th>id</th>
					<th>Date</th>
					<th>Type de panne</th>
					<th>Machine</th>
					<th>Type de machine</th>
					<th>Statut</th>
				</tr>
			<%
				for(Panne panne:liste4){
					int num = panne.getId();
					Date date =panne.getDate();
					String type = panne.getTypePanne();
					String machine = panne.getIdM();
					String typem = panne.getTmachine();
					String statut;
					if (panne.isPanRes()==false){
						statut="pas r�par�";
					} else {
						statut="r�par�";
					}
			%>
				<tr>
					<td><%= num %></td>
					<td><%= date %></td>
					<td><%= type %></td>
					<td><%= machine %></td>
					<td><%= typem %></td>
					<td><%= statut %>
					<%if (panne.isPanRes()==false){%>
					<input type="checkbox" name="repare" value="<%=num %>">
					
					
					</td>
					
					
					<%} %>
					</tr>
				<%} %>
			</table>
			<input type="submit" value="envoi" name="envoyer"/>
			</form>
		</div>
		<button id="detail4-t">Details par type</button>
		<div id="parmoistype">
			<form action="MonitoringServlet" method="post">
			<table border="1" cellpadding="5" cellspacing="0">
				<tr>
					<th>id</th>
					<th>Date</th>
					<th>Type de panne</th>
					<th>Machine</th>
					<th>Type de machine</th>
					<th>Statut</th>
				</tr>
			<%
				for(Panne panne:liste4t){
					int num = panne.getId();
					Date date =panne.getDate();
					String type = panne.getTypePanne();
					String machine = panne.getIdM();
					String typem = panne.getTmachine();
					String statut;
					if (panne.isPanRes()==false){
						statut="pas r�par�";
					} else {
						statut="r�par�";
					}
			%>
				<tr>
					<td><%= num %></td>
					<td><%= date %></td>
					<td><%= type %></td>
					<td><%= machine %></td>
					<td><%= typem %></td>
					<td><%= statut %>
					<%if (panne.isPanRes()==false){%>
					<input type="checkbox" name="repare" value="<%=num %>">
					</td>
					
					
					<%} %>
					</tr>
				<%} %>
			</table>
			<input type="submit" value="envoi" name="envoyer"/>
			</form>
		</div>
		
		</div>
		<div class="move">
		 <form action="PanneServlet" method="get">
        <label>
        <span>&nbsp;</span>  
        <input type="submit" class="button" value="Generateur de Pannes" name="gen" class="transfer"/> 
   		 </label> 
    	</form>
    	</div>
		<h3 id="titre">Cette page web se rafraichira dans <span id="compteurMinute">4</span> Minute(s) et <span id="compteurSeconde">59</span> seconde(s)</h3>
		<script type="text/javascript">
		var compteurSec = document.getElementById("compteurSeconde");
		var compteurMin = document.getElementById("compteurMinute");
		function diminuerCompteurSec() {

			var compteur = Number(compteurSec.textContent); 
			if(compteur<=0){     
				compteur=59;
			}
			compteurSec.textContent = compteur - 1;

		}
		setInterval(diminuerCompteurSec, 1000);
		function diminuerCompteurMin() {

			var compteur = Number(compteurMin.textContent); 
			if(compteur<=0){     
				compteur=4;
			}
			compteurMin.textContent = compteur - 1;
		}
		setInterval(diminuerCompteurMin, 60*1000);
		</script>
	</body>
<html>